﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;

namespace My_Text_Editor
{
    public partial class Form1 : Form
    {
        string Current_file="";
        find_and_replace findDialog;

        public Form1()
        {
            InitializeComponent();
            txtBody.Font = new Font("Arial", 14,FontStyle.Regular);
            this.FormClosed += Form1_FormClosed;
            UpdateStatusStrap();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

                if (Current_file == "")
                {
                    if(txtBody.Text!="")
                        txtSaveAs();
                }
                else if (isFileChanged())
                {
                    DialogResult d = MessageBox.Show("There is unsaved changes. Do you want to save last change ?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (d == DialogResult.Yes)
                        txtSave();
                }
            }

        private bool isFileChanged()
        {
            string temp;
            StreamReader s = new StreamReader(Current_file);
            temp = s.ReadToEnd();
            s.Close();

            if(temp==txtBody.Text)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d= openFileDialog1.ShowDialog();
            if(d!=DialogResult.Cancel)
            {
                StreamReader s = new StreamReader(openFileDialog1.FileName);
                Current_file = openFileDialog1.FileName;
                txtBody.Text= s.ReadToEnd();
                s.Close();
                
                this.Text = Current_file+" - Simple Text Editor";
            }

        }

     

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtSave();
        }

        private void txtSave()
        {
            if (Current_file != "")
            {
                StreamWriter s = new StreamWriter(Current_file);
                s.Write(txtBody.Text);
                s.Close();
            }
            else
                txtSaveAs();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtSaveAs();
        }

        private void txtSaveAs()
        {
            DialogResult d = saveFileDialog1.ShowDialog();
            if (d != DialogResult.Cancel)
            {
                StreamWriter s = new StreamWriter(saveFileDialog1.FileName);
                s.Write(txtBody.Text);
                s.Close();
                Current_file = saveFileDialog1.FileName;
                this.Text = Current_file + " - Simple Text Editor";
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtBody.Text = "";
            Current_file = "";
            UpdateStatusStrap();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtUndo(1);
           
        }

        private void txtUndo(int a)
        {
            if (a == 1)
                txtBody.Undo();
            else if (a == 2)
                txtBody.Redo();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            txtUndo(2);
        }

       

        private void timeDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtBody.SelectedText =DateTime.Now.ToShortTimeString()+" "+ DateTime.Now.ToShortDateString();
        }

        private void formatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void offToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtBody.WordWrap = false;
        }

        private void onToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtBody.WordWrap = true;
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d = fontDialog1.ShowDialog();
            if (d != DialogResult.Cancel)
                txtBody.Font = fontDialog1.Font;
        }

    

        private void fontToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult d = colorDialog1.ShowDialog();
            if (d != DialogResult.Cancel)
                txtBody.ForeColor = colorDialog1.Color;
        }

        private void backgroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d = colorDialog1.ShowDialog();
            if (d != DialogResult.Cancel)
                txtBody.BackColor = colorDialog1.Color;
        }

        private void statusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;
            if(t.Checked)
            {
                statusStrip1.Hide();
                t.Checked = false;
            }
            else
            {
                statusStrip1.Show();
                t.Checked = true;
            }
        }

        private void aboutTextEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Simple Text Editor, 2017.\nDesigned and developed by Ashwin Thapaliya", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pageSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d= printDialog1.ShowDialog();
            if(d!=DialogResult.Cancel)
            {
                PrintDocument pd = new PrintDocument();
                pd.DefaultPageSettings = new PageSetupDialog().PageSettings;
                pd.PrintPage += Pd_PrintPage;
                pd.Print();
            }
        }

        private void Pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(txtBody.Text.ToString(), txtBody.Font, Brushes.Black, 10, 10);
        }

        private void undoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtUndo(1);
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            txtUndo(2);
        }

        private void cutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtCut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtCopy();
        }

        private void txtCopy()
        {
            if (txtBody.SelectedText == "")
                Clipboard.SetText(txtBody.Text);
            else
                Clipboard.SetText(txtBody.SelectedText);
        }

        private void txtDelete()
        {
            if (txtBody.SelectedText == "")
                txtBody.Text = "";
            else
                txtBody.SelectedText = "";

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtDelete();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtPaste();
        }

        private void txtPaste()
        {
            txtBody.SelectedText = Clipboard.GetText();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtCut();
        }

        private void txtCut()
        {
            txtCopy();
            txtDelete();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtSelectAll();
        }

        private void txtSelectAll()
        {
            txtBody.SelectAll();
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtCopy();
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtPaste();
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtDelete();
        }

        private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            txtSelectAll();
        }

        private void UpdateStatusStrap()
        {
            string currentFile=(Current_file == "") ? "Not Saved" : Current_file;
            Letter.Text = "Letter: " + txtBody.TextLength;
            Word.Text = "Word: " + txtBody.Text.Split(' ').Length;
            Line.Text = "Line: " + txtBody.Text.Split('\n').Length;
            FileName.Text = "File: " + currentFile;
        }

        private void txtBody_TextChanged(object sender, EventArgs e)
        {
            UpdateStatusStrap();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (findDialog == null)
            {
                findDialog = new find_and_replace(this);
                findDialog.FormClosed += FindDialog_FormClosed;
                findDialog.Show();
            }
            else
            {
                findDialog.Activate();
            }

        }

        private void FindDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            findDialog = null;
        }

        private void findNextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            findToolStripMenuItem_Click(sender,e);
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            findToolStripMenuItem_Click(sender, e);
        }
    }
}
