﻿namespace My_Text_Editor
{
    partial class find_and_replace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbox_Find = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbox_Replace = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_findNext = new System.Windows.Forms.Button();
            this.btn_findAll = new System.Windows.Forms.Button();
            this.btn_Replace = new System.Windows.Forms.Button();
            this.btn_ReplaceAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtbox_Find
            // 
            this.txtbox_Find.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_Find.Location = new System.Drawing.Point(16, 27);
            this.txtbox_Find.Multiline = true;
            this.txtbox_Find.Name = "txtbox_Find";
            this.txtbox_Find.Size = new System.Drawing.Size(398, 38);
            this.txtbox_Find.TabIndex = 0;
            this.txtbox_Find.TextChanged += new System.EventHandler(this.txtbox_Find_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Find";
            // 
            // txtbox_Replace
            // 
            this.txtbox_Replace.Enabled = false;
            this.txtbox_Replace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtbox_Replace.Location = new System.Drawing.Point(16, 103);
            this.txtbox_Replace.Multiline = true;
            this.txtbox_Replace.Name = "txtbox_Replace";
            this.txtbox_Replace.Size = new System.Drawing.Size(398, 38);
            this.txtbox_Replace.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "And Replace With";
            // 
            // btn_findNext
            // 
            this.btn_findNext.Enabled = false;
            this.btn_findNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_findNext.Location = new System.Drawing.Point(3, 157);
            this.btn_findNext.Name = "btn_findNext";
            this.btn_findNext.Size = new System.Drawing.Size(76, 33);
            this.btn_findNext.TabIndex = 2;
            this.btn_findNext.Text = "Find";
            this.btn_findNext.UseVisualStyleBackColor = true;
            this.btn_findNext.Click += new System.EventHandler(this.btn_findNext_Click);
            // 
            // btn_findAll
            // 
            this.btn_findAll.Enabled = false;
            this.btn_findAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_findAll.Location = new System.Drawing.Point(85, 157);
            this.btn_findAll.Name = "btn_findAll";
            this.btn_findAll.Size = new System.Drawing.Size(120, 33);
            this.btn_findAll.TabIndex = 2;
            this.btn_findAll.Text = "Heighlight All";
            this.btn_findAll.UseVisualStyleBackColor = true;
            this.btn_findAll.Click += new System.EventHandler(this.btn_findAll_Click);
            // 
            // btn_Replace
            // 
            this.btn_Replace.Enabled = false;
            this.btn_Replace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Replace.Location = new System.Drawing.Point(211, 157);
            this.btn_Replace.Name = "btn_Replace";
            this.btn_Replace.Size = new System.Drawing.Size(85, 33);
            this.btn_Replace.TabIndex = 2;
            this.btn_Replace.Text = "Replace";
            this.btn_Replace.UseVisualStyleBackColor = true;
            this.btn_Replace.Click += new System.EventHandler(this.btn_Replace_Click);
            // 
            // btn_ReplaceAll
            // 
            this.btn_ReplaceAll.Enabled = false;
            this.btn_ReplaceAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ReplaceAll.Location = new System.Drawing.Point(302, 157);
            this.btn_ReplaceAll.Name = "btn_ReplaceAll";
            this.btn_ReplaceAll.Size = new System.Drawing.Size(111, 33);
            this.btn_ReplaceAll.TabIndex = 2;
            this.btn_ReplaceAll.Text = "Replace All";
            this.btn_ReplaceAll.UseVisualStyleBackColor = true;
            this.btn_ReplaceAll.Click += new System.EventHandler(this.btn_ReplaceAll_Click);
            // 
            // find_and_replace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 202);
            this.Controls.Add(this.btn_ReplaceAll);
            this.Controls.Add(this.btn_findAll);
            this.Controls.Add(this.btn_Replace);
            this.Controls.Add(this.btn_findNext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbox_Replace);
            this.Controls.Add(this.txtbox_Find);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "find_and_replace";
            this.ShowIcon = false;
            this.Text = "Find and Replace";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbox_Find;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbox_Replace;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_findNext;
        private System.Windows.Forms.Button btn_findAll;
        private System.Windows.Forms.Button btn_Replace;
        private System.Windows.Forms.Button btn_ReplaceAll;
    }
}