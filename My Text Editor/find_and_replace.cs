﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_Text_Editor
{
    public partial class find_and_replace : Form
    {
        public string text_to_find;
        public string text_to_replace;
        Form1 f;
        public find_and_replace(Form1 f)
        {
            InitializeComponent();
            this.f = f;
            f.txtBody.HideSelection = false;

        }

        private void txtbox_Find_TextChanged(object sender, EventArgs e)
        {
            

            if (txtbox_Find.Text != "")
            {
                btn_findNext.Enabled = true;
                btn_findAll.Enabled = true;
                txtbox_Replace.Enabled = true;
                btn_Replace.Enabled = true;
                btn_ReplaceAll.Enabled = true;
            }
            else
            {
                btn_findNext.Enabled = false;
                btn_findAll.Enabled = false;
                txtbox_Replace.Enabled = false;
                btn_Replace.Enabled = false;
                btn_ReplaceAll.Enabled = false;
            }
        }

        private void btn_findNext_Click(object sender, EventArgs e)
        {
            try
            {
                int len = txtbox_Find.Text.Length;
                int ind = f.txtBody.Text.IndexOf(txtbox_Find.Text);
                if (ind != -1)
                    f.txtBody.Select(ind, len);
                else
                    MessageBox.Show("No Word Found", "Not found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch { }
        }

        private void btn_ReplaceAll_Click(object sender, EventArgs e)
        {
            int found = HowMuch(txtbox_Find.Text);
            f.txtBody.Text = f.txtBody.Text.Replace(txtbox_Find.Text, txtbox_Replace.Text);
            MessageBox.Show(found + " words replaced successfully!", "Replace Complete");
        }

        private void btn_Replace_Click(object sender, EventArgs e)
        {
            if (f.txtBody.SelectedText != "")
                f.txtBody.SelectedText = txtbox_Replace.Text;
        }

        private void btn_findAll_Click(object sender, EventArgs e)
        {
            int Bodylength=f.txtBody.TextLength,word_length=txtbox_Find.Text.Length,startindex=0, index;
            int found = 0;
            while((index=f.txtBody.Text.IndexOf(txtbox_Find.Text,startindex))!=-1 && startindex<Bodylength)
            {
                f.txtBody.Select(index, word_length);
                f.txtBody.SelectionBackColor = Color.LightGreen;
                f.txtBody.SelectionColor = Color.White;
                startindex = index + word_length;
                ++found;
            }
            MessageBox.Show(found + " words found.", "Find Results");
        }
        private int HowMuch(string txt)
        {
            int found = 0,index,startindex=0;
            while ((index = f.txtBody.Text.IndexOf(txt, startindex)) != -1)
            {
                startindex = index + txt.Length;
                ++found;
            }
            return found;
        }
    }
}
